﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Robson_Construcoes
{
    class Program
    {
        static void Main(string[] args)
        {
            AddCargo addcargo = new AddCargo();
            AddFuncionario addfuncionario = new AddFuncionario();
            Relatorio relatorio = new Relatorio();
            TotalSalarios totalsalarios = new TotalSalarios();

            addcargo.NovoCargo(2500);
            addcargo.NovoCargo(1500);
            addcargo.NovoCargo(10000);
            addcargo.NovoCargo(1200);
            addcargo.NovoCargo(800);

            addfuncionario.NovoFuncionario(addcargo, 15, "Joao da Silva", 1);
            addfuncionario.NovoFuncionario(addcargo, 1, "Pedro Santos", 2);
            addfuncionario.NovoFuncionario(addcargo, 26, "Maria Oliveira", 3);
            addfuncionario.NovoFuncionario(addcargo, 12, "Rita Alcantara", 5);
            addfuncionario.NovoFuncionario(addcargo, 8, "Ligia Matos", 2);

            relatorio.MostrarRelatorio(addfuncionario);

            totalsalarios.ValorTotalPago(addcargo, addfuncionario, 1);

            Console.ReadLine();
        }

    }
}
