﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robson_Construcoes
{
    class Funcionario
    {
        private int codigo;
        private string nome;
        private int codigo_cargo;
        public int GetCodigo()
        {
            return codigo;
        }
        public string GetNome()
        {
            return nome;
        }
        public int GetCargo()
        {
            return codigo_cargo;
        }
        public Funcionario(int Codigo, string Nome, int Codigo_cargo)
        {
            codigo = Codigo;
            nome = Nome;
            codigo_cargo = Codigo_cargo;
        }        

    }
}
