﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robson_Construcoes
{
    class AddCargo
    {
        private List<Cargo> cargos = new List<Cargo>();

        public List<Cargo> GetCargos()
        {
        return cargos;
        }
        public void NovoCargo(float salario)
        {
            cargos.Add(new Cargo(salario));
        }
    }
}
