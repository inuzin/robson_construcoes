﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robson_Construcoes
{
    class TotalSalarios
    {
        AddCargo addcargo = new AddCargo();
        AddFuncionario addfuncionario = new AddFuncionario();

        public void ValorTotalPago(AddCargo addcargo, AddFuncionario addfuncionario, int codigo_cargo)
        {
            float ValorTotal = 0;
            //Confere se o cargo existe
            if (addcargo.GetCargos().Count() - 1 < codigo_cargo)
            {
                return;
            }
            //Soma o salario de todos os funcionarios do cargo, caso existam
            if (addfuncionario.GetFuncionarios().Count() - 1 != 0)
            {
                for(int i = 0; i <= addfuncionario.GetFuncionarios().Count() - 1; i++)
                {
                    if(addfuncionario.GetFuncionarios()[i].GetCargo() == codigo_cargo)
                    {
                        ValorTotal += addcargo.GetCargos()[codigo_cargo].GetSalario();
                    }
                }
            }

            Console.WriteLine("Total dos Salarios: " + ValorTotal);
        }
    }
}
